package com.fin.service;

import com.fin.entities.Event;
import com.fin.repos.EventRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class EventGenerator implements ApplicationRunner {

    @Autowired
    EventRepository eventRepository;

    @Override
    public void run(ApplicationArguments args) {
        if (args.getSourceArgs().length == 0 ||  !"load_db".equals(args.getSourceArgs()[0]))
            return;

        generate(60);

    }

    void generate(int intencity) {
        var current = new DateTime(2020, 1, 1, 0, 0 , 0);

        while (current.getYear() != 2021) {

            var sec = current.getMillis() + event(intencity);
            current = new DateTime(sec);

            var event = new Event("test", new Timestamp(sec),100, 1);
            eventRepository.save(event);

        }
    }

    private int event(int intencity) {
        return intencity;
    }
}
