package com.fin.controller;


import com.fin.entities.Event;
import com.fin.repos.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@RequestMapping("/event")
public class EventController {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Autowired
    private EventRepository eventRepository;

    @GetMapping("/all")
    public Iterable<Event> getEvents() {
        return eventRepository.findAll();
    }

    @GetMapping("/add/{name}/{value}/{type}")
    public void addEvent(@PathVariable("name") String name, @PathVariable("value") long value, @PathVariable("type") int type) {
        eventRepository.save(new Event(name, value, type));
    }

    @GetMapping("/add/{name}/{value}/{type}/{date}")
    public void addEvent(@PathVariable("name") String name, @PathVariable("value") long value, @PathVariable("type") int type, @PathVariable("date") String date) throws ParseException {
        var parsedDate = dateFormat.parse(date);
        Timestamp timestamp = new Timestamp(parsedDate.getTime());

        eventRepository.save(new Event(name, timestamp, value, type));
    }

    @GetMapping("/interval/{from}/{to}")
    public List<Event> getInterval(@PathVariable("from") String from, @PathVariable("to") String to) {
        return eventRepository.findByTime(from, to);
    }

    @GetMapping("/interval/manth/{delta}")
    public List<Event> getInterval(@PathVariable("delta") String delta) {
        return null;
    }

}
