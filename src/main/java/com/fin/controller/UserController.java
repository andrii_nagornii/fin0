package com.fin.controller;

import com.fin.entities.User0;
import com.fin.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/all")
    public Iterable<User0> getUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/add/{name}")
    public void addUser(@PathVariable("name") String name) {
        userRepository.save(new User0(name));
    }

}
