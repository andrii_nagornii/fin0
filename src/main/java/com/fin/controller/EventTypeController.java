package com.fin.controller;


import com.fin.entities.EventType;
import com.fin.entities.User0;
import com.fin.repos.EventTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event-type")
public class EventTypeController {

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @GetMapping("/all")
    public Iterable<EventType> getUsers() {
        return eventTypeRepository.findAll();
    }

    @GetMapping("/add/{name}/{type}")
    public void addEventType(@PathVariable("name") String name, @PathVariable("type") int type) {
        eventTypeRepository.save(new EventType(name, type));
    }

}
