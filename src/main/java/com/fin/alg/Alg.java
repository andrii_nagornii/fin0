package com.fin.alg;

import java.util.Arrays;

public class Alg {

    public long[] movingAverage(long[] data, int base, int future) {
        var result = new long[data.length + future];

        System.arraycopy(data, 0, result, 0, base);

        for (int i = base; i < data.length; i++) {
            result[i] = iteration(data, base, i);
        }

        for (int i = 0; (i < future) && (i < base); i++) {
            var aver0 = sum(data, data.length - base + i, data.length - 1);
            var aver1 = sum(result, data.length, data.length + i - 1);
            result[data.length + i] = (aver0 + aver1) / base;
        }

        for (int i = base; i < future; i++) {
            result[data.length + i] = iteration(result, base, data.length + i);
        }

        return result;
    }

    private long iteration(long[] arr, int base, int from) {
        return (long) Arrays
                .stream(arr, from - base, from)
                .average()
                .getAsDouble();
    }

    private long sum(long[] arr, int from, int to) {
        if (to < from) return 0;
        return Arrays
                .stream(arr, from, to + 1)
                .sum();
    }

}
