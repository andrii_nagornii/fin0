package com.fin.entities;

import org.springframework.data.annotation.Id;

import java.sql.Timestamp;

public class Event {
    @Id
    private int id;
    private String name;
    private Timestamp date0;
    private long value0;
    private int type;

    public Event(String name, long value0, int type) {
        this.name = name;
        this.value0 = value0;
        this.type = type;
        date0 = new Timestamp(System.currentTimeMillis());
    }

    public Event(String name, Timestamp timestamp, long value0, int type) {
        this.name = name;
        this.value0 = value0;
        this.type = type;
        date0 = timestamp;
    }

    public Event() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Timestamp getDate0() {
        return date0;
    }

    public void setDate0(Timestamp date0) {
        this.date0 = date0;
    }

    public long getValue0() {
        return value0;
    }

    public void setValue0(long value0) {
        this.value0 = value0;
    }
}
