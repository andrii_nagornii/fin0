package com.fin.entities;

import org.springframework.data.annotation.Id;

public class User0 {

    @Id
    private int id;

    private String name;

    public User0(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public User0(String name) {
        this.name = name;
    }

    public User0() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
