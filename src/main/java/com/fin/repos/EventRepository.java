package com.fin.repos;

import com.fin.entities.Event;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

    @Query("select * from EVENT where date0 between :from and :to order by date0")
    List<Event> findByTime(@Param("from") String from, @Param("to") String to);

    @Query("select * from EVENT where date0 between :from and :to order by date0")
    Event findMaxCredit(@Param("from") String from, @Param("to") String to);

    @Query("select MAX(value0) from EVENT where type = 1")
    long findMaxCredit();

    @Query("select * from EVENT where date0 between :from and :to order by date0")
    Event findMaxDebet(@Param("from") String from, @Param("to") String to);

    @Query("select MAX(value0) from EVENT where type = 0")
    long findMaxDebet();

}
