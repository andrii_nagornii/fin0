package com.fin.repos;

import com.fin.entities.User0;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User0, Long> {


}
