create table if not exists USER0 (
    id IDENTITY NOT NULL PRIMARY KEY,
    name varchar(20) NOT NULL UNIQUE
);

create table if not exists EVENT_TYPE (
    id IDENTITY NOT NULL PRIMARY KEY,
    name varchar(20) NOT NULL,
    type int DEFAULT 0
);

create table if not exists EVENT (
    id IDENTITY NOT NULL PRIMARY KEY,
    name varchar(20) NOT NULL,
    type int DEFAULT 0,
    value0 bigint,
    date0 TIMESTAMP
);

insert into USER0 values (0, 'Andrey');
insert into USER0 values (1, 'Olena');
insert into USER0 values (2, 'Vera');
insert into USER0 values (3, 'Ivan');

insert into EVENT_TYPE values (0, 'TYPE_0', 1);
insert into EVENT_TYPE values (1, 'TYPE_1', 0);
insert into EVENT_TYPE values (2, 'TYPE_2', 0);
insert into EVENT_TYPE values (3, 'TYPE_3', 1);

insert into EVENT values (0, 'EVENT_0', 1, 150, '2000-05-15 03:05:30');
insert into EVENT values (1, 'EVENT_1', 1, 150, '2000-05-15 03:05:31');