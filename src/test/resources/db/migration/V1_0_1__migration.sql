create table if not exists USER0 (
    id IDENTITY NOT NULL PRIMARY KEY,
    name varchar(20) NOT NULL UNIQUE
)

create table if not exists EVENT_TYPE (
    id IDENTITY NOT NULL PRIMARY KEY,
    name varchar(20) NOT NULL,
    type int DEFAULT 0
)

create table if not exists EVENT (
    id IDENTITY NOT NULL PRIMARY KEY,
    name varchar(20) NOT NULL,
    type int DEFAULT 0,
    value0 bigint,
    date0 TIMESTAMP
)