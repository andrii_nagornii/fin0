package com.fin.controller;

import com.fin.repos.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.stream.StreamSupport;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    UserRepository userRepository;

    @Test
    void test() {
        restTemplate.getForObject("http://localhost:" + port + "/user/add/Olena", Void.class);
        restTemplate.getForObject("http://localhost:" + port + "/user/add/Andrey", Void.class);
        var res = restTemplate.getForObject("http://localhost:" + port + "/user/all", Iterable.class);

        var count = StreamSupport.stream(res.spliterator(), false).count();

        Assertions.assertEquals(2, count);
    }

    @AfterEach
    void after() {
        userRepository.deleteAll();
    }

}
