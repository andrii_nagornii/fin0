package com.fin.controller;

import com.fin.entities.Event;
import com.fin.repos.EventRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.Map;
import java.util.stream.StreamSupport;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EventControllerTest {

    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    EventRepository eventRepository;

    @Test
    void testAdd() {
        restTemplate.getForObject("http://localhost:" + port + "/event/add/test0/100/1", Void.class);
        restTemplate.getForObject("http://localhost:" + port + "/event/add/test1/100/0", Void.class);
        var res = restTemplate.getForObject("http://localhost:" + port + "/event/all", Iterable.class);

        var count = StreamSupport.stream(res.spliterator(), false).count();

        Assertions.assertEquals(2, count);
    }

    @Test
    void testAddWithTime() {
        restTemplate.getForObject("http://localhost:" + port + "/event/add/test0/100/1/2000-05-15 03:05:30", Void.class);
        restTemplate.getForObject("http://localhost:" + port + "/event/add/test1/100/0/2000-05-15 03:06:30", Void.class);
        restTemplate.getForObject("http://localhost:" + port + "/event/add/test1/100/0/2000-05-15 03:07:30", Void.class);
        var res = restTemplate.getForObject("http://localhost:" + port + "/event/all", Iterable.class);

        var arr = StreamSupport
                .stream(res.spliterator(), false)
                .map(event -> ((Map) event).get("date0"))
                .toArray();

        Assertions.assertArrayEquals(new String[]{"2000-05-15T00:05:30.000+00:00", "2000-05-15T00:06:30.000+00:00", "2000-05-15T00:07:30.000+00:00"}, arr);
    }

    @AfterEach
    void after() {
        eventRepository.deleteAll();
    }
}
