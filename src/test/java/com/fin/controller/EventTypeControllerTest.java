package com.fin.controller;

import com.fin.repos.EventTypeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.stream.StreamSupport;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EventTypeControllerTest {

    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    EventTypeRepository eventTypeRepository;


    @Test
    void test() {
        restTemplate.getForObject("http://localhost:" + port + "/event-type/add/test0/1", Void.class);
        restTemplate.getForObject("http://localhost:" + port + "/event-type/add/test1/0", Void.class);
        var res = restTemplate.getForObject("http://localhost:" + port + "/event-type/all", Iterable.class);

        var count = StreamSupport.stream(res.spliterator(), false).count();

        Assertions.assertEquals(2, count);
    }

    @AfterEach
    void after() {
        eventTypeRepository.deleteAll();
    }

}
