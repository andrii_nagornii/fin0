package com.fin.service;


import com.fin.repos.EventRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.StreamSupport;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EventGeneratorTest {

    @Autowired
    EventGenerator eventGenerator;

    @Autowired
    EventRepository eventRepository;

    @Test
    void test() {
        eventGenerator.generate(60 * 60 * 24 * 1000);

        long result = StreamSupport
                .stream(eventRepository.findAll().spliterator(), false)
                .count();

        Assertions.assertEquals(366, result);
    }

    @AfterEach
    void after() {
        eventRepository.deleteAll();
    }

}
