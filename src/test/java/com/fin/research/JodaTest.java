package com.fin.research;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class JodaTest {

    DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm:ss");

    @Test
    void test() {
        var dt = DateTime.parse("2000-05-15 03:05:30", format);
        var interval = monthInterval(dt.getMillis(), 0);

        assertArrayEquals(new String[]{"2000-05-01 01:00:00", "2000-05-15 03:05:30"}, interval);
    }

    private String[] monthInterval(long current, int delta) {
        var dt = new DateTime(current);

        int year = dt.getYear();
        int month = dt.getMonthOfYear() + delta;
        int day = 1;
        int hour = 1;
        int minute = 0;
        int second = 0;

        var from = new DateTime(year, month, day, hour, minute, second);

        return new String[]{from.toString("yyyy-MM-dd hh:mm:ss"), dt.toString("yyyy-MM-dd hh:mm:ss")};
    }

}
