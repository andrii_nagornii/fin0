package com.fin.alg;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AlgTest {

    Alg alg = new Alg();

    @Test
    public void movingAverageTest0() {
        var data = new long[]{10, 10, 13, 12, 14, 14, 16, 18};
        var res = alg.movingAverage(data, 3, 1);

        assertArrayEquals(res, new long[]{10, 10, 13, 11, 11, 13, 13, 14, 16});
    }

    @Test
    public void movingAverageTest1() {
        var data = new long[]{62, 70, 72};
        var res = alg.movingAverage(data, 3, 2);

        assertArrayEquals(res, new long[]{62, 70, 72, 68, 70});
    }

    @Test
    public void movingAverageTest2() {
        var data = new long[]{62, 70, 72};
        var res = alg.movingAverage(data, 3, 3);

        assertArrayEquals(res, new long[]{62, 70, 72, 68, 70, 70});
    }

    @Test
    public void movingAverageTest3() {
        var data = new long[]{62, 70, 72};
        var res = alg.movingAverage(data, 3, 4);

        assertArrayEquals(res, new long[]{62, 70, 72, 68, 70, 70, 69});
    }

    @Test
    public void movingAverageTest_LARGE() {
        var data = new Random().longs(1000).toArray();
        var res = alg.movingAverage(data, 100, 100);

        assertEquals(data.length + 100, res.length);
    }

}
