package com.fin.repos;

import com.fin.entities.EventType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.StreamSupport;

@SpringBootTest
public class EventTypeRepositoryTest {

    @Autowired
    EventTypeRepository eventTypeRepository;

    @Test
    public void test() {
        eventTypeRepository.save(new EventType("debet", 0));
        eventTypeRepository.save(new EventType("credet", 1));

        var spliterator = eventTypeRepository.findAll().spliterator();
        var count = StreamSupport.stream(spliterator, false).count();

        Assertions.assertEquals(2, count);
    }

    @AfterEach
    void after() {
        eventTypeRepository.deleteAll();
    }

}
