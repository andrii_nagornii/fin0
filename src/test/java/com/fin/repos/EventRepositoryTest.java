package com.fin.repos;

import com.fin.entities.Event;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@SpringBootTest
class EventRepositoryTest {

    @Autowired
    EventRepository eventRepository;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Test
    void testBase() {
        eventRepository.save(new Event("test0",100, 1));
        eventRepository.save(new Event("test1",150, 0));
        eventRepository.save(new Event("test2",200, 0));

        var events = toList(eventRepository.findAll());
        Assertions.assertEquals(3, events.size());
    }

    @Test
    void testFindByTime() throws ParseException {
        var date0 = timestamp("2000-06-01 02:00:00");
        eventRepository.save(new Event("test0", date0, 100, 1));

        var date1 = timestamp("2000-06-01 01:59:00");
        eventRepository.save(new Event("test1", date1,150, 0));

        var date2 = timestamp("2000-06-01 02:01:00");
        eventRepository.save(new Event("test2", date2,200, 0));

        var first = timestamp("2000-06-01 01:58:00");
        eventRepository.save(new Event("first", first,150, 0));

        var last = timestamp("2000-06-01 02:02:00");
        eventRepository.save(new Event("last", last,200, 0));

        var events = toList(eventRepository.findByTime("2000-06-01 01:58:30", "2000-06-01 02:01:30"));
        var result = events.stream()
                .map(Event::getDate0)
                .toArray();

        Assertions.assertArrayEquals(new Timestamp[]{date1, date0, date2}, result);

    }

    @Test
    void testMaxCredit() {
        eventRepository.save(new Event("test0",100, 1));
        eventRepository.save(new Event("test1",150, 0));
        eventRepository.save(new Event("test2",200, 0));
        eventRepository.save(new Event("test0",700, 1));
        eventRepository.save(new Event("test0",100, 1));

        var max = eventRepository.findMaxCredit();
        Assertions.assertEquals(700, max);
    }

    @Test
    void testMaxDebet() {
        eventRepository.save(new Event("test0",100, 1));
        eventRepository.save(new Event("test1",150, 0));
        eventRepository.save(new Event("test2",200, 0));
        eventRepository.save(new Event("test0",700, 1));
        eventRepository.save(new Event("test0",100, 1));

        var max = eventRepository.findMaxDebet();
        Assertions.assertEquals(200, max);
    }

    private Timestamp timestamp(String asString) throws ParseException {
        var date0 = dateFormat.parse(asString);
        return new Timestamp(date0.getTime());
    }

    private List<Event> toList(Iterable<Event> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
    }

    @AfterEach
    void after() {
        eventRepository.deleteAll();
    }

}
