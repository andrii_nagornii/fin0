package com.fin.repos;

import com.fin.entities.User0;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.StreamSupport;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void test() {
        userRepository.save(new User0("Andrey"));
        userRepository.save(new User0("Olena"));

        var spliterator = userRepository.findAll().spliterator();
        var count = StreamSupport.stream(spliterator, false).count();
        Assertions.assertEquals(2, count);
    }

    @AfterEach
    void after() {
        userRepository.deleteAll();
    }

}
